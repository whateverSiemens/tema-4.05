#include "..\..\..\include\spring\Application\BaseScene.h"

namespace Spring
{
	BaseScene::BaseScene(const std::string& ac_szSceneName) : IScene(ac_szSceneName)
	{
	}

	void BaseScene::createScene()
	{
		std::string appName = boost::any_cast<std::string>(m_TransientDataCollection["ApplicationName"]);

		m_uMainWindow->setWindowTitle(QString(appName.c_str()));
		createGUI();


		QObject::connect(backButton, SIGNAL(released()), this, SLOT(mf_BackButton()));
	}

	void BaseScene::release()
	{
		delete centralWidget;
	}
	
	BaseScene::~BaseScene()
	{
	}
	void BaseScene::createGUI()
	{
		m_uMainWindow->resize(410, 288);
		centralWidget = new QWidget(m_uMainWindow.get());
		centralWidget->setObjectName(QStringLiteral("centralWidget"));
		gridLayout = new QGridLayout(centralWidget);
		gridLayout->setSpacing(6);
		gridLayout->setContentsMargins(11, 11, 11, 11);
		gridLayout->setObjectName(QStringLiteral("gridLayout"));
		verticalLayout_2 = new QVBoxLayout();
		verticalLayout_2->setSpacing(6);
		verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
		customPlot = new QCustomPlot(centralWidget);
		customPlot->setObjectName(QStringLiteral("customPlot"));
		

		verticalLayout_2->addWidget(customPlot);

		horizontalLayout = new QHBoxLayout();
		horizontalLayout->setSpacing(6);
		horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
		horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

		horizontalLayout->addItem(horizontalSpacer);

		startButton = new QPushButton(centralWidget);
		startButton->setObjectName(QStringLiteral("startButton"));

		horizontalLayout->addWidget(startButton);

		stopButton = new QPushButton(centralWidget);
		stopButton->setObjectName(QStringLiteral("stopButton"));

		horizontalLayout->addWidget(stopButton);

		backButton = new QPushButton(centralWidget);
		backButton->setObjectName(QStringLiteral("backButton"));

		horizontalLayout->addWidget(backButton);

		verticalLayout_2->addLayout(horizontalLayout);


		gridLayout->addLayout(verticalLayout_2, 2, 0, 1, 1);

		m_uMainWindow->setCentralWidget(centralWidget);

		startButton->setText(QApplication::translate("MainWindow", "Start", Q_NULLPTR));
		stopButton->setText(QApplication::translate("MainWindow", "Stop", Q_NULLPTR));
		backButton->setText(QApplication::translate("MainWindow", "Back", Q_NULLPTR));
	}

	void BaseScene::mf_BackButton()
	{

		const std::string c_szNextSceneName = "InitialScene";
		emit SceneChange(c_szNextSceneName);
	}
}
