#pragma once
#include <spring\Framework\IScene.h>
#include <QtWidgets/QApplication>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include <qcustomplot.h>
namespace Spring
{
	class BaseScene : public IScene
	{
		Q_OBJECT
	public:

		explicit BaseScene(const std::string& ac_szSceneName);

		void createScene() override;

		void release() override;
		void createGUI();
		~BaseScene();
	private:
		QWidget *centralWidget;
		QGridLayout *gridLayout;
		QVBoxLayout *verticalLayout_2;
		QCustomPlot *customPlot;
		QHBoxLayout *horizontalLayout;
		QSpacerItem *horizontalSpacer;
		QPushButton *startButton;
		QPushButton *stopButton;
		QPushButton *backButton;

		public slots:
		void mf_BackButton();
	};

}
